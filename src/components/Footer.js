import React from 'react'

export default function Footer() {
  return (
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
           Plataforma desarrolla desde Innova ID. licencia CC BY NC SA 4.0.
        </p>
      </div>
    </footer>
  )
}
