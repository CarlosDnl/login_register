import React, { Fragment } from 'react';
import Hero from './Hero';
import HomeContent from './HomeContent';

export default function Home() {
  return (
    <Fragment>
      <Hero />
      <div className="box cta">
        <p className="has-text-centered">
          <span className="tag is-primary">Nuevo</span> La plataforma de INNOVA ID ahora funciona con los servicios de AWS
        </p>
      </div>
      <HomeContent />
    </Fragment>
  )
}
