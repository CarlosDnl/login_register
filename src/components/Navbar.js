import React, { Component } from 'react';
import { Auth } from 'aws-amplify';

export default class Navbar extends Component {
  handleLogOut = async event => {
    event.preventDefault();
    try {
      Auth.signOut();
      this.props.auth.setAuthStatus(false);
      this.props.auth.setUser(null);
    }catch(error) {
      console.log(error.message);
    }
  }
  render(){
    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            <img src="hexal-logo.png" width="112" height="28" alt="hexal logo" />
          </a>
        </div>

        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <a href="/" className="navbar-item">
              Inicio
            </a>
            <a href="/products" className="navbar-item">
              Productos
            </a>
            <a href="/admin" className="navbar-item">
              Administrador
            </a>
          </div>

          <div className="navbar-end">
            <div className="navbar-item">
              {this.props.auth.isAuthenticated && this.props.auth.user && (
                <p>
                  Hola {this.props.auth.user.username}
                </p>
              )}
              <div className="buttons">
                {!this.props.auth.isAuthenticated && (
                  <div>
                    <a href="/register" className="button is-primary">
                      <strong>Registro</strong>
                    </a>
                    <a href="/login" className="button is-light">
                      Ingresar
                    </a>
                  </div>
                )}
                {this.props.auth.isAuthenticated && (
                  <a href="/login" onClick={this.handleLogOut} className="button is-light">
                    Salir
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}


