import React from 'react';

export default function Welcome() {
  return (
    <section className="section auth">
      <div className="container">
        <h1>Bienvenido!</h1>
        <p>El registro fue realizado correctamente.</p>
        <p>Ingresa a tu dirección de correo electrónico y da clic en el enlace para verificar la cuenta</p>
      </div>
    </section>
  )
}
