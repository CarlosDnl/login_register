import React, { Component } from "react";

class ChangePasswordConfirmation extends Component {
  render() {
    return (
      <section className="section auth">
        <div className="container">
          <h1>Cambiar contraseña</h1>
          <p>Tu contraseña fue actualizada correctamente</p>
        </div>
      </section>
    );
  }
}

export default ChangePasswordConfirmation;